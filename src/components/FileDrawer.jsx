import React from "react";
import {
  Drawer,
  DrawerBody,
  DrawerOverlay,
  DrawerContent,
  DrawerCloseButton,
  useDisclosure,
  IconButton,
  Tooltip,
} from "@chakra-ui/react";
import { FcOpenedFolder } from "react-icons/fc";

import FileHeader from "./FileHeader";

function FileDrawer(props) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const btnRef = React.useRef();

  return (
    <div style={{ width: "100%" }}>
      <Tooltip hasArrow label="Manage files and folders" bg="red.600">
        <IconButton
          ref={btnRef}
          onClick={onOpen}
          variant="unstyled"
          aria-label="Call Sage"
          fontSize="30px"
          icon={<FcOpenedFolder />}
        />
      </Tooltip>
      <Drawer
        isOpen={isOpen}
        placement="left"
        onClose={onClose}
        autoFocus={false}
        closeOnOverlayClick={false}
      >
        <DrawerOverlay />
        <DrawerContent>
          <div style={{ display: "flex", paddingLeft: "7px" }}>
            <DrawerCloseButton />
            <FileHeader />
          </div>

          <DrawerBody></DrawerBody>
        </DrawerContent>
      </Drawer>
    </div>
  );
}

export default FileDrawer;
