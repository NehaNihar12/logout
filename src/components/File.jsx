import React, { Component } from "react";

class File extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputText: this.props.file.name,
    };
    console.log(this.state.inputText);
  }
  handleActiveFile = (file) => {
    console.log(file.id);
    this.props.handleActiveFile(file.id);
  };
  handleKeyUp = (e) => {
    //e.preventDefault();
    this.setState({ inputText: e.currentTarget.textContent });
    if (e.key === "Enter") {
      this.props.renameItem(this.state.inputText);
    }
  };

  render() {
    const { file, activeButton } = this.props;
    console.log(activeButton);

    return (
      <div
        contentEditable={activeButton === "Rename" ? "true" : "false"}
        style={{ fontSize: "16px" }}
        onClick={() => {
          this.handleActiveFile(file);
        }}
        onKeyUp={this.handleKeyUp}
      >
        {file.name}
      </div>
    );
  }
}

export default File;
