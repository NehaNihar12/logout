import React, { Component } from "react";
import { connect } from "react-redux";

import * as actionType from "../store/actions";
import {
  ButtonGroup,
  IconButton,
  Tooltip,
  Input,
  List,
  ListItem,
} from "@chakra-ui/react";
import { VscNewFile, VscNewFolder, VscTrash, VscEdit } from "react-icons/vsc";

import File from "./File";
import Folder from "./Folder";

class FileHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
      activeButton: this.props.activeButton,
    };
  }

  //controlled input element
  handleChange = (e) => {
    this.setState({ value: e.target.value });
  };

  //on submitting new filename
  handleSubmit = (e) => {
    e.preventDefault();
    if (this.state.value !== "") {
      if (this.props.activeButton === "newFile") {
        this.props.addFile(this.state.value);
      } else if (this.props.activeButton === "newFolder") {
        this.props.addFolder(this.state.value);
      }
      this.setState({ value: "" });
    }
  };

  render() {
    const { fileDir, activeButton, activeFileId } = this.props;
    console.log(activeButton);

    return (
      <div>
        <ButtonGroup justifyvalue="center" size="sm" pt="6px" pb="3px" px="0">
          <Tooltip hasArrow label="New File" bg="red.600">
            <IconButton
              size="sm"
              icon={
                <VscNewFile
                  onClick={() => this.props.onActivateButton("newFile")}
                />
              }
              value="newFile"
            />
          </Tooltip>
          <Tooltip hasArrow label="New Folder" bg="red.600">
            <IconButton
              size="sm"
              icon={
                <VscNewFolder
                  value="newFolder"
                  onClick={() => this.props.onActivateButton("newFolder")}
                />
              }
            />
          </Tooltip>
          <Tooltip hasArrow label="Delete" bg="red.600">
            <IconButton
              size="sm"
              icon={
                <VscTrash
                  value="Delete"
                  onClick={() => this.props.delete(this.props.activeFileId)}
                />
              }
            />
          </Tooltip>
          <Tooltip hasArrow label="Rename" bg="red.600">
            <IconButton
              size="sm"
              ml="auto"
              icon={
                <VscEdit
                  value="Rename"
                  onClick={() => this.props.onActivateButton("Rename")}
                />
              }
            />
          </Tooltip>
        </ButtonGroup>
        {this.props.activeButton === "newFile" ||
        this.props.activeButton === "newFolder" ? (
          <form onSubmit={this.handleSubmit}>
            <Input
              value={this.state.value}
              onChange={this.handleChange}
              placeholder="new name..."
              size="sm"
              mr="2rem"
            />
          </form>
        ) : null}
        <List spacing={3}>
          {fileDir.map((obj) =>
            obj.type === "file" ? (
              <ListItem
                className={
                  obj.id === this.props.activeFileId
                    ? "list fillActive"
                    : "list"
                }
              >
                <File
                  file={obj}
                  activeButton={activeButton}
                  handleActiveFile={(activeFileId) =>
                    this.props.handleActiveFile(activeFileId)
                  }
                  renameItem={(newName) => this.props.renameItem(newName)}
                ></File>
              </ListItem>
            ) : (
              <ListItem
                className={obj.id === activeFileId ? "list fillActive" : "list"}
              >
                <Folder
                  folder={obj}
                  activeButton={activeButton}
                  handleActiveFile={(activeFileId) =>
                    this.props.handleActiveFile(activeFileId)
                  }
                  renameItem={(newName) => this.props.renameItem(newName)}
                ></Folder>
              </ListItem>
            )
          )}
        </List>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    activeButton: state.activeButton,
    activeFileId: state.activeFileId,
    fileDir: state.fileDir,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addFile: (name) =>
      dispatch({
        type: actionType.ADD_FILE,
        payloads: {
          name: name,
        },
      }),
    addFolder: (name) =>
      dispatch({
        type: actionType.ADD_FOLDER,
        payloads: {
          name: name,
        },
      }),

    handleActiveFile: (activeFileId) =>
      dispatch({
        type: actionType.ACTIVATED_FILE_ID,
        payloads: {
          activeFileId: activeFileId,
        },
      }),

    delete: (activeFileId) =>
      dispatch({
        type: actionType.DELETE_ITEM,
        payloads: {
          activeFileId: activeFileId,
        },
      }),
    onActivateButton: (activeButton) => {
      dispatch({
        type: actionType.ACTIVATE_BUTTON,
        payloads: {
          activeButton: activeButton,
        },
      });
    },
    renameItem: (newName) => {
      dispatch({
        type: actionType.RENAME_ITEM,
        payloads: {
          newName: newName,
        },
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FileHeader);
