import React, { Component } from "react";
// import { FaThemeisle } from "react-icons/fa";

class StatusBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: this.props.value1,
      //bytes: "",
    };
  }

  componentDidUpdate = () => {
    if (this.state.content !== this.props.value1) {
      this.setState({
        content: this.props.value1,
      });
    }
  };

  // handleChange = (e) => {
  //     console.log(e.target.value1)
  //     this.setState({
  //         text: e.target.value1
  //     })
  // }

  render() {
    console.log(this.state.content);
    console.log(this.props.value1);
    return (
      <React.Fragment>
        {/* <div className="text-area">
                    <label>Input Text</label>
                    <textarea
                        name="textvalue1"
                        rows="5"
                        column="5"
                        onChange={this.handleChange}
                    />
                </div> */}

        <footer className="status-Bar">
          <div className="left-Panel">
            <span className="left-panel-label">Markdown</span>
            <span className="left-panel-spans">
              {this.state.content.length}
            </span>
            <label className="left-panel-label">bytes</label>
            <span className="left-panel-spans">
              {this.state.content.split(" ").length}
            </span>
            <label className="left-panel-label">words</label>
            <span className="left-panel-spans">
              {this.state.content.split("\n").length}
            </span>
            <label className="left-panel-label">lines</label>
            {/* <span className="left-panel-spans">ln {this.state.content.rowIndex},col {this.state.content.colIndex}</span> */}
          </div>
          <div className="right-Panel">
            <span className="right-panel-label">HTML</span>
            <span className="right-panel-spans">
              {(this.state.content.match(/[a-z]/g) || []).length}
            </span>
            <label className="right-panel-label">characters</label>
            <span className="right-panel-spans">
              {this.state.content.split(" ").length}
            </span>
            <label className="right-panel-label">words</label>
            <span className="right-panel-spans">
              {this.state.content.split("\n\n").length}
            </span>
            <label className="right-panel-label">paragraphs</label>
          </div>
        </footer>
      </React.Fragment>
    );
  }
}

export default StatusBar;
