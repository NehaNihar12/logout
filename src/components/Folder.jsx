import React, { Component } from "react";
import {
  Box,
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
} from "@chakra-ui/react";

class Folder extends Component {
  handleActiveFile = (folder) => {
    //console.log(file);
    this.props.handleActiveFile(folder.id);
  };
  handleKeyUp = (e) => {
    //e.preventDefault();
    this.setState({ inputText: e.currentTarget.textContent });
    if (e.key === "Enter") {
      this.props.renameItem(this.state.inputText);
    }
  };
  render() {
    const { folder, activeButton } = this.props;
    return (
      <Accordion allowToggle>
        <AccordionItem>
          <h2>
            <AccordionButton>
              <Box
                contentEditable={activeButton === "Rename" ? "true" : "false"}
                flex="1"
                textAlign="left"
                onClick={() => {
                  this.handleActiveFile(folder);
                }}
                onKeyUp={this.handleKeyUp}
              >
                {folder.name}
              </Box>
              <AccordionIcon />
            </AccordionButton>
          </h2>
          <AccordionPanel pb={4}>{"file here"}</AccordionPanel>
        </AccordionItem>
      </Accordion>
    );
  }
}

export default Folder;
