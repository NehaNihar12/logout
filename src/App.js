import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";

import "./App.css";
import FrontPage from "./components/FrontPage";
import FileDrawer from "./components/FileDrawer";
import Markdown from "./container/Markdown";

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Switch>
          <Route
            path="/App"
            render={(props) => (
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  flexFlow: "row wrap",
                }}
              >
                <FileDrawer />
                <Markdown />
              </div>
            )}
          />

          <Route exact path="/" component={FrontPage} />
        </Switch>
      </React.Fragment>
    );
  }
}

export default App;
