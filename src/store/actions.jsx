export const ADD_FILE = "ADD_FILE";
export const ADD_FOLDER = "ADD_FOLDER";
export const DELETE_ITEM = "DELETE_ITEM";
export const RENAME_ITEM = "RENAME_ITEM";
export const ADD_CONTENT = "ADD_CONTENT";
export const ACTIVATED_FILE_ID = "ACTIVE_FILE_ID";
export const ACTIVATE_BUTTON = "ACTIVATE_BUTTON";
