import * as actionType from "../actions";
import { v4 as uuidv4 } from "uuid";

const initialState = {
  fileDir: [
    {
      key: uuidv4(),
      id: uuidv4(),
      name: "hello world",
      content: "hello",
      createdAt: new Date(),
      type: "file",
    },
  ],
  activeFileId: null,
  activeButton: "",
  value: "*hello*",
};

const filemanagerReducer = (state = initialState, action) => {
  console.log(state, action);
  switch (action.type) {
    case actionType.ADD_FILE:
      return {
        ...state,
        fileDir: [
          ...state.fileDir,
          {
            key: uuidv4(),
            id: uuidv4(),
            name: action.payloads.name,
            content: "hello",
            createdAt: new Date(),
            type: "file",
          },
        ],
        activeButton: "",
      };
    case actionType.ADD_FOLDER:
      return {
        ...state,
        fileDir: [
          ...state.fileDir,
          {
            key: uuidv4(),
            id: uuidv4(),
            name: action.payloads.name,
            content: "hello",
            createdAt: new Date(),
            type: "folder",
          },
        ],
        activeButton: "",
      };
    case actionType.DELETE_ITEM: {
      const newFileDir = state.fileDir.filter(
        (file) => file.id !== action.payloads.activeFileId
      );
      return {
        ...state,
        fileDir: newFileDir,
        activeButton: "",
      };
    }
    case actionType.RENAME_ITEM: {
      const newdir = state.fileDir.filter((obj) => {
        if (obj.id === action.payloads.activeFileId) {
          obj.name = action.payloads.newName;
        }
        return obj;
      });
      return {
        ...state,
        fileDir: newdir,
        activeButton: "",
      };
    }
    case actionType.ACTIVATED_FILE_ID:
      return {
        ...state,
        activeFileId: action.payloads.activeFileId,
      };
    case actionType.ACTIVATE_BUTTON:
      return {
        ...state,
        activeButton: action.payloads.activeButton,
      };

    case actionType.ADD_CONTENT: {
      console.log(action.payloads.value);
      const newDir = state.fileDir.map((obj) => {
        if (obj.id === state.activeFileId) {
          obj = { ...obj, content: action.payloads.value };
        }
        return obj;
      });
      return { ...state, fileDir: newDir };
    }
    default:
      return state;
  }
};
export default filemanagerReducer;
