import { combineReducers } from "redux";
import markdown from "./markdownReducer";
import fileManager from "./filemanagerReducer";

export default combineReducers({
  markdown,
  fileManager,
});
