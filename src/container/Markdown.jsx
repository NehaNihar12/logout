import React from "react";
// import ReactDOM from "react-dom";
// , { commands, ICommand, TextState, TextAreaTextApi, image }
import StatusBar from "../components/StatusBar";
import MDEditor, { commands } from "@uiw/react-md-editor";
import { FaStrikethrough } from "react-icons/fa";
import { FaBold } from "react-icons/fa";
import { FaItalic } from "react-icons/fa";
import { GrUnorderedList } from "react-icons/gr";
import { GoListOrdered } from "react-icons/go";
import { MdChecklist } from "react-icons/md";
import { FiLink } from "react-icons/fi";
import { BsImage } from "react-icons/bs";
import { FaHeading } from "react-icons/fa";
import { GoCode } from "react-icons/go";
import { GrBlockQuote } from "react-icons/gr";
// import { useState } from "react";
import { connect } from "react-redux";
import * as actionType from "../store/actions";

const title1 = {
  name: "title1",
  keyCommand: "title1",
  buttonProps: { "aria-label": "Insert title1" },
  icon: <FaBold style={{ fontSize: "17px", fontWeight: "400" }} />,
  execute: (state, api) => {
    let modifyText1 = `**${state.selectedText}**`;
    if (!state.selectedText) {
      modifyText1 = `**`;
    }
    api.replaceSelection(modifyText1);
  },
};

const title2 = {
  name: "title2",
  keyCommand: "title2",
  buttonProps: { "aria-label": "Insert title2" },
  icon: <FaItalic style={{ fontSize: "17px", fontWeight: "400" }} />,
  execute: (state, api) => {
    let modifyText2 = `*${state.selectedText}*`;
    if (!state.selectedText) {
      modifyText2 = `*`;
    }
    api.replaceSelection(modifyText2);
  },
};

const title3 = {
  name: "title3",
  keyCommand: "title3",
  buttonProps: { "aria-label": "Insert title3" },
  icon: <FaStrikethrough style={{ fontSize: "17px", fontWeight: "400" }} />,
  execute: (state, api) => {
    let modifyText3 = `~~${state.selectedText}~~`;
    if (!state.selectedText) {
      modifyText3 = ``;
    }
    api.replaceSelection(modifyText3);
  },
};

const title4 = {
  name: "title4",
  keyCommand: "title4",
  buttonProps: { "aria-label": "Insert title4" },
  icon: <FaHeading style={{ fontSize: "17px", fontWeight: "400" }} />,
  execute: (state, api) => {
    let modifyText4 = `### ${state.selectedText}`;
    if (!state.selectedText) {
      modifyText4 = `### `;
    }
    api.replaceSelection(modifyText4);
  },
};

const title5 = {
  name: "title5",
  keyCommand: "title5",
  buttonProps: { "aria-label": "Insert title5" },
  icon: <GrUnorderedList style={{ fontSize: "17px", fontWeight: "400" }} />,
  execute: (state, api) => {
    let modifyText5 = `* ${state.selectedText}`;
    if (!state.selectedText) {
      modifyText5 = `* `;
    }
    api.replaceSelection(modifyText5);
  },
};

const title6 = {
  name: "title6",
  keyCommand: "title6",
  buttonProps: { "aria-label": "Insert title6" },
  icon: <GoListOrdered style={{ fontSize: "17px", fontWeight: "400" }} />,
  execute: (state, api) => {
    let modifyText6 = `1. ${state.selectedText}`;
    if (!state.selectedText) {
      modifyText6 = `1. `;
    }
    api.replaceSelection(modifyText6);
  },
};

const title7 = {
  name: "title7",
  keyCommand: "title7",
  buttonProps: { "aria-label": "Insert title7" },
  icon: <MdChecklist style={{ fontSize: "17px", fontWeight: "400" }} />,
  execute: (state, api) => {
    let modifyText7 = `- [] ${state.selectedText}`;
    if (!state.selectedText) {
      modifyText7 = `- [] `;
    }
    api.replaceSelection(modifyText7);
  },
};

const title8 = {
  name: "title8",
  keyCommand: "title8",
  buttonProps: { "aria-label": "Insert title8" },
  icon: <GoCode style={{ fontSize: "17px", fontWeight: "400" }} />,
  execute: (state, api) => {
    let modifyText8 = `\`\`\`\n ${state.selectedText}\n \`\`\``;
    if (!state.selectedText) {
      modifyText8 = ``;
    }
    api.replaceSelection(modifyText8);
  },
};

const title9 = {
  name: "title9",
  keyCommand: "title9",
  buttonProps: { "aria-label": "Insert title9" },
  icon: <GrBlockQuote style={{ fontSize: "17px", fontWeight: "400" }} />,
  execute: (state, api) => {
    let modifyText9 = `> ${state.selectedText}`;
    if (!state.selectedText) {
      modifyText9 = `>`;
    }
    api.replaceSelection(modifyText9);
  },
};

// const title12 = {
//     name: 'title12',
//     keyCommand: 'title12',
//     buttonProps: { 'aria-label': 'Insert title12' },
//     icon: (
//         <img src="/logos/Table.png" alt="Img logo"></img>
//     ),
//     execute: (state, api) => {
//         let modifyText12 = `
//     | Title1         | Title2                |
//     |----------------|-----------------------|
//     | Text1          | Text2                 |${state.selectedText}`;
//         if (!state.selectedText) {
//             modifyText12 = `
//     | Title1         | Title2                |
//     |----------------|-----------------------|
//     | Text1          | Text2                 |`;
//         }
//         api.replaceSelection(modifyText12);
//     },
// };

class Markdown extends React.Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
    this.state = {
      //fileDirectory: this.props.fileDir,
      value: "",
    };
  }

  render() {
    console.log(this.props);
    return (
      <div className="container">
        <MDEditor
          style={{ width: "100vw", minHeight: "37rem" }}
          onChange={(e) =>
            this.props.dispatch({
              type: actionType.ADD_CONTENT,
              payloads: { value: e },
            })
          }
          value={this.props.value}
          commands={[
            // Custom Toolbars
            title1,
            title2,
            title4,
            title3,
            title5,
            title6,
            title7,
            title8,
            title9,

            // Pop-up Toolbars
            commands.group([], {
              name: "update1",
              groupName: "update",
              icon: <FiLink style={{ fontSize: "17px", fontWeight: "400" }} />,
              children: ({ close, execute, getState, textApi }) => {
                // console.log("AP");
                return (
                  <div className="pop-up-outer-div" style={{ width: 400 }}>
                    <button onClick={() => close()} className="pop-up-X-button">
                      X
                    </button>
                    <div className="pop-up-url-div">
                      <p>Please provide a URL</p>
                      <label className="pop-up-URL-label">URL</label>
                      <input
                        type="text"
                        ref={this.myRef}
                        className="pop-up-input"
                        placeholder="Input URL"
                      ></input>
                    </div>
                    <div className="pop-up-buttons-div">
                      <button onClick={() => close()} className="Cancel-button">
                        Cancel
                      </button>
                      <button
                        onClick={() => {
                          execute();
                          close();
                        }}
                        className="OK-button"
                      >
                        OK
                      </button>
                    </div>
                  </div>
                );
              },
              execute: (state, api) => {
                console.log(">>>>>>update>>>>>", state, api);
                const inputElem = this.myRef.current;
                console.log(inputElem.value);
                if (!inputElem || inputElem.value.trim().length === 0) return;
                let modifyText10 = `[Enter link description](${inputElem.value})`;
                api.replaceSelection(modifyText10);
                inputElem.value = "";
              },
              buttonProps: { "aria-label": "Insert title" },
            }),

            commands.group([], {
              name: "update2",
              groupName: "update",
              icon: <BsImage Style={{ fontSize: "17px", fontWeight: "400" }} />,
              children: ({ close, execute, getState, textApi }) => {
                return (
                  <div className="pop-up-outer-div" style={{ width: 400 }}>
                    <button onClick={() => close()} className="pop-up-X-button">
                      X
                    </button>
                    <div className="pop-up-url-div">
                      <p>Please provide a URL</p>
                      <label className="pop-up-URL-label">URL</label>
                      <input
                        type="text"
                        ref={this.myRef}
                        className="pop-up-input"
                        placeholder="Input URL"
                      ></input>
                    </div>
                    <div className="pop-up-buttons-div">
                      <button onClick={() => close()} className="Cancel-button">
                        Cancel
                      </button>
                      <button
                        onClick={() => {
                          execute();
                          close();
                        }}
                        className="OK-button"
                      >
                        OK
                      </button>
                    </div>
                  </div>
                );
              },
              execute: (state, api) => {
                console.log(">>>>>>update>>>>>", state, api);
                const inputElem = this.myRef.current;
                console.log(inputElem.value);
                if (!inputElem || inputElem.value.trim().length === 0) return;
                let modifyText = `![Enter link description](${inputElem.value})`;
                api.replaceSelection(modifyText);
                inputElem.value = "";
              },
              buttonProps: { "aria-label": "Insert title" },
            }),
          ]}
        />

        <StatusBar value1={this.props.value} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    value: state.value,
  };
};

export default connect(mapStateToProps)(Markdown);
